@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Lead') }}</div>

                    <div class="card-body">
                            <table class="table table-hover">
                                <thead>
                                    @yield('headers')
                                </thead>
                                <tbody>
                                    @yield('body')
                                </tbody>
                            </table>
                    </div>
                </div>
                <div class="card">

                    <div class="card-body">
                        @yield('paginate')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
