@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Lead') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('lead-store') }}">
                            @csrf

                            @form_group(['name'=>__('email'),'type'=>'email','label'=>'Email'])

                            @form_group(['name'=>__('name'),'type'=>'text','label'=>'Name'])

                            @form_group(['name'=>__('surname'),'type'=>'text','label'=>'Surname'])

                            @form_group(['name'=>__('phone'),'type'=>'tel','label'=>'Phone'])

                            @form_group(['name'=>__('description'),'type'=>'textarea','label'=>'Description', 'required'=>false])

                            @form_group(['name'=>__('type'),'type'=>'select', 'options' => $leadTypes,'label'=>'Type'])

                            @button(['name'=>__('Send')])

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
