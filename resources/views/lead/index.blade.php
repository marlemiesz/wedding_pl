@extends('layouts.listing')

@section('headers')
    <tr>
        <th>{{ __('Name') }}</th>
        <th>{{ __('Surname') }}</th>
        <th>{{ __('Phone') }}</th>
        <th>{{ __('Action') }}</th>
    </tr>
@endsection

@section('body')
    @if($leads)
        @foreach($leads as $lead)

            <tr>
                <td>{{ $lead['name'] }}</td>
                <td>{{ $lead['surname'] }}</td>
                <td>{{ $lead['phone'] }}</td>
                <td>
                    <a href="{{ route('lead-show',$lead['id']) }}" class="btn btn-info">{{ __('Show') }}</a>
                </td>
            </tr>

        @endforeach
    @endif
@endsection

@section('paginate')
    {{$leads->links()}}
@endsection


