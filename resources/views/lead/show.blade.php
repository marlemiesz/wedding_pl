@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Lead') }}</div>

                    <div class="card-body">

                        <table class="table table-hover">
                            <thead>
                                <th>{{ __('Name') }}</th>
                                <th>{{ __('Value') }}</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{ __('Name') }}</td>
                                    <td>{{ $lead['name'] }}</td>
                                </tr>
                                <tr>
                                    <td>{{ __('Surname') }}</td>
                                    <td>{{ $lead['surname'] }}</td>
                                </tr>
                                <tr>
                                    <td>{{ __('Phone') }}</td>
                                    <td>{{ $lead['phone'] }}</td>
                                </tr>
                                <tr>
                                    <td>{{ __('Type') }}</td>
                                    <td>{{ $leadTypes[$lead['type']] }}</td>
                                </tr>
                                <tr>
                                    <td>{{ __('Description') }}</td>
                                    <td>{!! $lead['description'] !!}</td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="float-right">
                            <a href="{{route('lead-index')}}" class="btn btn-warning">{{ __('Return') }}</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
