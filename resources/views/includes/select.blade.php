<select id="{{ $name ?? 'empty' }}" type="{{ $type ?? 'text' }}"
       class="form-control @error($name ?? 'empty') is-invalid @enderror" name="{{ $name ?? 'empty' }}"
       {{ !isset($required) || $required === true ? 'required' : '' }} autocomplete="{{ $name ?? 'empty' }}">
    <option value="">{{ __('Please choose option') }}</option>
    @foreach($options as $key=>$option)
        <option @if(old($name ?? 'empty') === $key) selected @endif value="{{ $key }}">{{ $option }}</option>
    @endforeach
</select>
