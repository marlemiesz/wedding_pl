<div class="form-group row">
    <div class="col-6 offset-4 text-right">
        <button type="submit" class="btn btn-primary ">
            {{ $name ?? 'Save' }}
        </button>
    </div>
</div>
