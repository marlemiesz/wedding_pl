<div class="form-group row">
    <label for="email" class="col-md-4 col-form-label text-md-right">{{ $label ?? 'empty' }}</label>

    <div class="col-md-6">
        @if($type === 'textarea')
            @textarea(['name' => $name])
        @elseif($type === 'select')
            @select(['name' => $name, 'options' => $options ?? [] ])
        @else
            @input(['type' => $type ?? 'text', 'name' => $name])
        @endif
        @error($name)
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
