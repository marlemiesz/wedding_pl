<input id="{{ $name ?? 'empty' }}" type="{{ $type ?? 'text' }}"
       class="form-control @error($name ?? 'empty') is-invalid @enderror" name="{{ $name ?? 'empty' }}"
       value="{{ old($name ?? 'empty') }}"
       {{ !isset($required) || $required === true ? 'required' : '' }} autocomplete="{{ $name ?? 'empty' }}" autofocus>
