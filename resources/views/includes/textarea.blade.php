<textarea id="{{ $name ?? 'empty' }}" class="form-control @error($name ?? 'empty') is-invalid @enderror"
          name="{{ $name ?? 'empty' }}"
          {{ !isset($required) || $required === true ? 'required' : '' }} autofocus> {{ old($name ?? 'empty') }} </textarea>
