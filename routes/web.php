<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function () {

    Route::get('/admin', 'LeadController@index')->name('lead-index');
    Route::get('/admin/{id}/show', 'LeadController@show')->name('lead-show');

});

Route::get('/', 'LeadController@create')->name('lead-create');

Route::post('/lead', 'LeadController@store')->name('lead-store');


Auth::routes(['register' => false]);


