## RUN IN DEV

- composer install
- npm install && npm run dev
- configure .env file (DB and SMTP e.g. mailtrap)
- php artisan key:generate
- php artisan migrate
- php artisan db:seed
- php artisan serve

#### Information

- Default user: interview@wedding.pl / wedding123
- Leads: https://drive.google.com/file/d/1LXn9dsxxDkGB-r3iVQ_DJ0-oaetdI4n7/view?usp=sharing
- Leads create: https://drive.google.com/file/d/1KbtMsIRVTev4etIuGIujenWHnUxASW0z/view?usp=sharing
- Leads listing: https://drive.google.com/file/d/14BizoUFwb03caGKSIkBg5IBcK7qAlHrO/view?usp=sharing
- Leads card: https://drive.google.com/file/d/1XSuGejKAiIycuVcFrwOOsv8EqxtKcvcF/view?usp=sharing
- Leads notification: https://drive.google.com/file/d/13emSwGRF3Q3MgOKDaMcECG-zzHVatLYa/view?usp=sharing
- Leads statistic command: statistic:generate => https://drive.google.com/file/d/1njWS7ywdPQz9pSGHI7xteJOWSO6wa_Zz/view?usp=sharing
