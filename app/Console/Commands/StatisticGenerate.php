<?php

namespace App\Console\Commands;

use App\Repository\LeadRepositoryInterface;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;

class StatisticGenerate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'statistic:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * @var LeadRepositoryInterface
     */
    private $leadRepository;

    /**
     * Create a new command instance.
     *
     * @param LeadRepositoryInterface $leadRepository
     */
    public function __construct(LeadRepositoryInterface $leadRepository)
    {
        parent::__construct();
        $this->leadRepository = $leadRepository;
    }

    protected function renderTable(Collection $leads){

        if(!$leads->first()){
            $this->error('Leads not exist');
        }

        $headers = array_keys($leads->first()->toArray());

        $this->table($headers, $leads->toArray());
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->anticipate('How to group count of leads [Daily, Monthly]?', ['Daily', 'Monthly'], 'Monthly');

        if($type === 'Daily'){
            $leadsGrouped = $this->leadRepository->getCountGroupByDaily();
        }
        else if($type === 'Monthly'){
            $leadsGrouped = $this->leadRepository->getCountGroupByMonth();
        }
        else {
            $this->error('Incorrect choose');
            exit;
        }

        $this->renderTable($leadsGrouped);


    }
}
