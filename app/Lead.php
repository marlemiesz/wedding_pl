<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{

    protected $fillable = ['name', 'surname', 'email', 'phone', 'description', 'type'];

    /**
     * @return array
     */
    public function getType(): array
    {
        return $this->type;
    }
}
