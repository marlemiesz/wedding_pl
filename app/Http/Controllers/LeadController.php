<?php

namespace App\Http\Controllers;

use App\Http\Requests\LeadRequest;
use App\Repository\LeadRepositoryInterface;
use App\Wedding\Decorator\LeadTypesDecoratorNames;

class LeadController extends Controller
{

    private $limitPaginate = 50;

    /**
     * @var LeadRepositoryInterface
     */
    private $leadRepository;

    /**
     * LeadController constructor.
     * @param LeadRepositoryInterface $leadRepository
     */
    public function __construct(LeadRepositoryInterface $leadRepository)
    {
        $this->leadRepository = $leadRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('lead.index')
            ->with('leads', $this->leadRepository->orderByCreatedAt()->paginate($this->limitPaginate))
            ;


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('lead.create')
            ->with('leadTypes', (new LeadTypesDecoratorNames($this->leadRepository))->getTypes())
            ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(LeadRequest $request)
    {
        $entry = $this->leadRepository->create($request->toArray());

        if($entry){

            $request->session()->flash('success', __('Lead was created'));

            return redirect(route('lead-create'));
        }

        $request->session()->flash('error', __('Unexpected error.'));

        return redirect(route('lead-create'));
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('lead.show')
            ->with('lead', $this->leadRepository->findOrFail($id))
            ->with('leadTypes', (new LeadTypesDecoratorNames($this->leadRepository))->getTypes())
            ;
    }


}
