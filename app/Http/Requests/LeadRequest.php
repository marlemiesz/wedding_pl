<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LeadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|min:3',
            'email'=>'required|email|unique:leads',
            'surname'=>'required|min:3',
            'type'=>'required',
            'phone'=>'required|regex:/^\d{3}-\d{3}-\d{3}$/',
        ];
    }

    public function messages()
    {
        return [
          'phone.regex'  => __('Invalid phone number. Please use pattern 123-123-123'),
        ];
    }

}
