<?php
namespace App\Repository;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface LeadRepositoryInterface
{
    public function all(): Collection;


    /**
     * @return Collection|null
     */
    public function paginate(): ?LengthAwarePaginator ;

    /**
     * @return Builder
     */
    public function orderByCreatedAt(): Builder;

    public function getCountGroupByMonth(): Collection;

    public function getCountGroupByDaily(): Collection;
}
