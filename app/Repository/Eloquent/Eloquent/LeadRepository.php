<?php

namespace App\Repository\Eloquent;

use App\Lead;
use App\Repository\Eloquent\LeadRepository\LeadRepositoryTypes;
use App\Repository\LeadRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use DB;

class LeadRepository extends BaseRepository implements LeadRepositoryInterface, LeadRepositoryTypes
{

    protected $types = [
        'wedding_dress',
        'suit',
        'catering',
        'place'
    ];
    /**
     * UserRepository constructor.
     * @param Lead $model
     */
    public function __construct(Lead $model)
    {
        parent::__construct($model);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->model->all();
    }

    /**
     * @return array
     */
    public function getTypes(): array
    {
        return $this->types;
    }


    /**
     * @inheritDoc
     */
    public function paginate(): ?LengthAwarePaginator
    {
        return $this->model->paginate();
    }


    public function orderByCreatedAt(): Builder
    {
        return $this->model->orderBy('created_at', 'DESC');
    }

    public function getCountGroupByMonth(): Collection
    {
        return $this->model->select(DB::raw('count(id) as `count`'), DB::raw("DATE_FORMAT(created_at, '%m-%Y') date"),  DB::raw('YEAR(created_at) year, MONTH(created_at) month'))
        ->groupby('year','month')
        ->get();
    }

    public function getCountGroupByDaily(): Collection
    {
        return $this->model->select(DB::raw('count(id) as `count`'), DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y') date"),  DB::raw('YEAR(created_at) year, MONTH(created_at) month, DAY(created_at) day'))
            ->groupby('year','month','day')
            ->get();
    }
}
