<?php
namespace App\Repository\Eloquent\LeadRepository;

interface LeadRepositoryTypes {

    /**
     * @return array
     */
    public function getTypes(): array;

}
