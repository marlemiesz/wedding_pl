<?php

namespace App\Wedding\Decorator;

class LeadTypesDecoratorNames extends LeadTypesDecorator
{


    /**
     * @return array
     */
    public function getTypes(): array
    {
        $data = [];

        $types = $this->lead->getTypes();

        foreach ($types as $type) {
            if ($type === 'wedding_dress') {
                $data[$type] = __('Wedding dress');
            } else if ($type === 'suit') {
                $data[$type] = __('Suit');
            } else if ($type === 'catering') {
                $data[$type] = __('Catering');
            } else if ($type === 'place') {
                $data[$type] = __('Place');
            } else {
                $data[$type] = $type;
            }
        }

        return $data;

    }
}
