<?php
namespace App\Wedding\Decorator;

use App\Repository\Eloquent\LeadRepository\LeadRepositoryTypes;
use App\Repository\LeadRepositoryInterface;

abstract class LeadTypesDecorator implements LeadRepositoryTypes {

    /**
     * @var LeadRepositoryInterface
     */
    protected $lead;

    public function __construct(LeadRepositoryTypes $lead)
    {
        $this->lead = $lead;
    }


}
