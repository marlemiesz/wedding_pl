<?php

namespace App\Providers;

use App\Repository\Eloquent\LeadRepository;
use App\Repository\LeadRepositoryInterface;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EloquentRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(LeadRepositoryInterface::class, LeadRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::include('includes.select', 'select');
        Blade::include('includes.input', 'input');
        Blade::include('includes.textarea', 'textarea');
        Blade::include('includes.form-group', 'form_group');
    }
}
