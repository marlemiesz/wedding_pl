<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::include('includes.select', 'select');
        Blade::include('includes.input', 'input');
        Blade::include('includes.button', 'button');
        Blade::include('includes.textarea', 'textarea');
        Blade::include('includes.form-group', 'form_group');
    }
}
