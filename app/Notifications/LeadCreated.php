<?php

namespace App\Notifications;

use App\Lead;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class LeadCreated extends Notification
{
    use Queueable;
    /**
     * @var Lead
     */
    private $lead;

    /**
     * Create a new notification instance.
     *
     * @param Lead $lead
     */
    public function __construct(Lead $lead)
    {
        //
        $this->lead = $lead;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {



        return (new MailMessage)
                    ->line('The new lead was created.')
                    ->line(sprintf ('Id: %d',$this->lead->id))
                    ->line(sprintf ('Name: %s',$this->lead->name))
                    ->line(sprintf ('Surname: %s',$this->lead->surname))
                    ->line(sprintf ('Description: %s',$this->lead->description))
                    ->line(sprintf ('Phone: %s',$this->lead->phone))
                    ->action(__('Show lead'), url(route('lead-show', $this->lead->id)))
            ;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
