<?php

namespace App\Observers;

use App\Lead;
use App\Notifications\LeadCreated;
use App\User;

class LeadObserver
{
    /**
     * Handle the lead "created" event.
     *
     * @param \App\Lead $lead
     * @return void
     */
    public function created(Lead $lead)
    {
        // We have only one admin at the database
        // TODO <mar.lemiesz@gmail.com> => I can make get user from DB more pretty :) , but it just an example how I deal with notification.
        $user = User::find(1);
        $user->notify(new LeadCreated($lead));

    }

}
