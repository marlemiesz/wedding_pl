<?php

use App\User as User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash as Hash;

class UserSeeder extends Seeder
{
    private $email = 'interview@wedding.pl';
    private $password = 'wedding123';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(User $user)
    {
        $user->create([
            'name' => 'wedding_pl',
            'email' => $this->email,
            'password' => Hash::make($this->password)
        ]);
    }
}
