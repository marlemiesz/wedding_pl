<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Lead;
use App\Repository\Eloquent\LeadRepository as LeadRepository;
use Faker\Generator as Faker;
Config::set('mail.driver', 'log');
$factory->define(Lead::class, function (Faker $faker) {
    return array(
        'name' => $faker->name,
        'surname' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->phoneNumber,
        'description' => $faker->paragraph,
        'created_at' => $faker->dateTimeBetween('-30 days'),
        'type' => $faker->randomElement((new LeadRepository((new Lead)))->getTypes())
    );
});
